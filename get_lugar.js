import mysql from 'mysql';

export default function get_lugar(req) {
    //1. Definir parametros de conexion mysql

    let conexion;

    let parametros = {
        host: 'localhost',
        user: 'jose',
        password: '123456789',
        database: 'wallettrip_db'
    }

    conexion = mysql.createConnection(parametros);

    //2. Conectarnos al servidor mysql
    conexion.connect(function(err) {
        if (err) {
            console.log("error" + err.message);
            return false;
        } else {
            console.log("Conectado al servidor MySQL");
            return true;
        }
    });

    //3. Realizar consulta sql

    let consulta = `SELECT idplace, name FROM place WHERE idplace=?`;
    var vglobal;

    conexion.query(consulta, [req.query.id], (err, results, fields) => {

        if (err) {
            console.error("error" + err.message)
        } else {
            console.log(results);
            vglobal = JSON.stringify(results);
        }


    });


    //4. Desconectarnos al servidor mysql

    conexion.end();


    return vglobal;



}