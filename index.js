import express from 'express';
import get_lugar from './get_lugar.js';

const app = express();

let puerto = 3005;

//Definir ruta

app.get("/get_lugar", (req, res) => {
    res.send(get_lugar(req));
});

// Iniciar un servidor

app.listen(puerto, () => { console.log("Esta funcionando el servidor") });